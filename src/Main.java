public class Main {
    public static void main(String[] args) {

        Personne personne = new Personne.Builder()
                .setPrenom("Chat")
                .setNom("Tech")
                .setAge(18)
                .build();

        System.out.println(personne.getPrenom());
        System.out.println(personne.getNom());
        System.out.println(personne.getAge());
        System.out.println(personne.getAddresse());

    }
}