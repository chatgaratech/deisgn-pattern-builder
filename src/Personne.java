public class Personne {

    private final String prenom;
    private final String nom;
    private final int age;
    private final String addresse;

    private Personne(Builder builder) {
        this.prenom = builder.prenom;
        this.nom = builder.nom;
        this.age = builder.age;
        this.addresse = builder.addresse;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public int getAge() {
        return age;
    }

    public String getAddresse() {
        return addresse;
    }

    public static class Builder {
        private String prenom;
        private String nom;
        private int age;
        private String addresse;

        public Builder setPrenom(String prenom) {
            this.prenom = prenom;
            return this;
        }

        public Builder setNom(String nom) {
            this.nom = nom;
            return this;
        }

        public Builder setAge(int age) {
            this.age = age;
            return this;
        }

        public Builder setAddresse(String addresse) {
            this.addresse = addresse;
            return this;
        }

        public Personne build() {
            return new Personne(this);
        }
    }
}
