# design-pattern-builder

Description

* Exemple simple sur la création d'un builder (Design Pattern Builder)

Auteur 

* Chatgara-Tech

Chaine Youtube :

* https://www.youtube.com/@Chatgara-Tech


Sources : 

* https://refactoring.guru/design-patterns/builder/java/example
* https://www.digitalocean.com/community/tutorials/builder-design-pattern-in-java